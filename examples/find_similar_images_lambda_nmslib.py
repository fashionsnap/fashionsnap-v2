
'''

This code can be used to replicate the production process of taking a cropped image, embedding, and finding similar images.


'''


from fashionsnap.embedding.object import LambdaEmbeddingObject
from fashionsnap.similarity.search import NMSLIBSearchConnection, rollup_image_scores_to_item_level
from fashionsnap.detection.detector import ImageBBoxSet
from fashionsnap.dataset import CSVImageDataset

from settings import IMAGES_CSV, IMAGE_METADATA_CSV


### NOTE: THIS IS BASICALLY THE SAME AS THE OTHER SCRIPT IN THE EXAMPLES FOLDER!!!!!
# delete one of them!


imagedataset = CSVImageDataset({'imagefileloc':IMAGES_CSV, 'itemfileloc':IMAGE_METADATA_CSV})





imageid = 'purse_only.jpg'
user_bbox_dict = {}
user_bbox_dict['top'] = .03
user_bbox_dict['left'] = .05
user_bbox_dict['bottom'] = .7
user_bbox_dict['right'] = .88

ubboxset = detector.ImageBBoxSet.create_from_user_crop(imageid, bbox_dict)


# TODO: save crop and use new imageid (and new basepath?)


embeddingobj = LambdaEmbeddingObject()
embedarray = embeddingobj.create_embedding(imageid)


# searchobj = NMSLIBSearchConnection()
# imageid_score_dict = searchobj.query_sim_server(embedarray)

# mock up of search obj
imageids = ['9066aa262c16e7bbcd85749c66a1855b', '6f86b8b3b091425d0e46223b62f65d19', '619a24429fd8c7c626c160bc37e3aba2', '891a8f061cb91392e0a3a196b7a3c2ac', 'a98bd0dfb71a6c6994fa1e2b3d18b89e', 
'b1b57ccd9b94280f3b54918976795070', '2b73e754e32d6f2864b1955a78c001c8', '199cb0b4e458bfc1aeab3aadbe2c4dfe', '8477258570e2b213f1889e7f07b96d98', 'a332201c85a38b6d6bad88dbc5f679be', 
'cb26653e8a1a17d24775b47098c7b824', 'd1dbf6c40e3f175ea0d965998fb4a4c6', 'e50c1a6ab87746926611fbbbcb27536b', '481612960680851a584399b725cba303', '056bc0235d1b5ea4d266197040e76e4e', 
'fda0de3a9b03f052447f67d1ba747bc6', '061882fc40315822e759e8a759814419', '148d45c052db09e2dbdc0173c99aef39', '2a6a668b9015b8b7dfa83dc0b17c2525', '30f63ed7607049324e37613e0e1f6d48', 
'3d74feee7a4888d362115062d1b0273f', 'fdf983498fd4948be986e7c99ee6e3bd', 'f81e5180ee32cb7526a982d1072dd1ea', '078d1ee4e3fe24c2d24e314fc1d7e27a', '245abf8da114e4a4eda5c42d89c57674', 
'bd200cfb136c6cc2f62d246567f1d1c8', 'efb724980099a7addc14f3e580681ae4', '42c24b39f7f56eb285fc9d2de675a67e', '006f5eb938864402507a9c44567efcb6', '6a7b26fde7b8b2a5c6bf71316eeeccde', 
'7a3978974b2c43afa1829293e5b877da', '95c0c3c65e9ef9bebf1acb07ef1b871c', '6795141c9d2557f4ffbd36ac005aba67', '13b86a773837289e1dadc9064400d3c6', '6cc4eb00faeb27b5556b579dd970e0c9', 
'6cf03ed8bab1ac757a8ef75601583d5c', 'b9a5ba413a7c828ec812458e07f85658', '7ad46541f2799bd0ae441e978d1edadf', 'fee7a0850001ec1a0e5593074d74f114', '211c0c6724498a7d5c8ff8275eb93b19', 
'5e36d1ff695d41e93bb8d529642ee732', '7a273d6398e04b48abf804baeb5e49db', '8c1a572c9cd2da6e5c3fba0a49c9605a', 'b692515e2a92ec9a36d1e46dfaf08f1e', 'b9e60f40aae4ac8820d1655769beafcb']
scores = [random.random() for _ in range(45)]
imageid_score_dict = dict(zip(imageid,scores))


# take {imageid:score, imageid:score} and add all metadata info
metadata = imagedataset.get_image_info(imageid_score_dict.keys())
for m in metadata: m['score'] = imageid_score_dict[m['imageid']]


# combine to get final list to pass to browser (items)
similar_items = rollup_image_scores_to_item_level(metadata)


import ipdb;ipdb.set_trace()
