''' 

Script that mimics what its like when an image comes in from the web from a user


'''


import itertools

from fashionsnap.dataset import CSVImageDataset
from fashionsnap.detection import detector
from fashionsnap.embedding.object import LocalEmbeddingObject, EmbeddingObject
from fashionsnap.similarity.search import MockSearchConnection

from settings import IMAGES_CSV, IMAGE_METADATA_CSV


### SETUP

# onetime setup of model and dataset connection
linksdict = {'imagefileloc':IMAGES_CSV, 'itemfileloc':IMAGE_METADATA_CSV}
dataset = CSVImageDataset(linksdict)
vgg = VGG()
embedobj = LocalEmbeddingObject(vgg)



### RUNNING

# take imageid and boundbox coords and create bbox object
imageid = 'purse_only.jpg'
user_bbox_dict = {}
user_bbox_dict['top'] = .03
user_bbox_dict['left'] = .05
user_bbox_dict['bottom'] = .7
user_bbox_dict['right'] = .88

ubboxset = detector.ImageBBoxSet.create_from_user_crop(imageid, bbox_dict)

# create embedding
embedding = embedobj.create_embedding(imgid)

# search embedding
searchconn = MockSearchConnection()
imageid_score_dict = searchconn.query_sim_server(embedding)

# get data in format to return
imageinfo = dataset.get_image_info(imageid_score_dict.keys())
for m in imageinfo: m['score'] = imageid_score_dict[m['imageid']]

similar_items = rollup_image_scores_to_item_level(imageinfo)
