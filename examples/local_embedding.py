


from fashionsnap.embedding.object import LocalEmbeddingObject
from fashionsnap.embedding.model import CustomInceptionV3



imageid = 'purse_only.jpg'

model = CustomInceptionV3()
embeddingobj = LocalEmbeddingObject(model)

embeddingarray = embeddingobj.create_embedding(imageid)


