''' 

Script to create bounding boxes, crop, and embed large numbers of images 


'''


import itertools

from fashionsnap.util.aws import get_spark
from fashionsnap.detection import detector
from fashionsnap.embedding.object import LocalEmbeddingObject, EmbeddingObject
from fashionsnap.similarity.search import NMSLIBSearchFile



EMBED_MATRIX_SAVE_LOC = ''
EMBED_FILE_LOC = ''
EMBED_MAP_FILE_LOC = ''





def spark_bbox_all(imageidchunks):
	# imagechunks = [[imageid,imgpath,category],[...],[...]]
	tf_graph_path = '/home/taylor/Programs/fashionsnap-v2/data/models/output_inference_graph-rfcn.pb'

	object_detector = detector.TensorflowObjectDetector(tf_graph_path)
	imagebboxsets = object_detector.batch_bbox(imageidchunks)
	imagebboxsets = detector.filter_valid_imagebboxsets(imagebboxsets)

	return imagebboxsets


def spark_crop_all(imagebboxsets):

	allcropfilenames = []
	for imagebboxset in imagebboxsets:
		cropfilenames = imagebboxset.crop_all(self, save=True, keep_in_memory=False)
		allcropfilenames.extend(cropfilenames)

	return allcropfilenames


def spark_embed_all(imageids):
	
	embeddingobj = LocalEmbeddingObject()
	embedarraylist = embeddingobj.embed_all(imageids)

	return embedarraylist


def embedarraylist_to_embedmatrix(embedarraylist, save=False):

	embeddingarraylist = itertools.chain.from_iterable(rdd.collect())
	embedmatrix = EmbeddingObject.create_embedding_matrix(embeddingarraylist)

	if save:
		EmbeddingObject.save_embedding_matrix(embedmatrix,EMBED_MATRIX_SAVE_LOC)

	return embedmatrix





spark = get_spark()

data = []
partitions = max(data//20000,100)

rdd = spark.parallelize(data,partitions)
rdd = rdd.mapPartitions(spark_bbox_all)
rdd = rdd.flatMap(spark_crop_all)
rdd = rdd.mapPartitions(spark_embed_all)

embeddingarraylist = itertools.chain.from_iterable(rdd.collect())
embedmatrix = embedarraylist_to_embedmatrix(embedarraylist, save=True)

embeddingid_map = NMSLIBSearchFile.create_embedding_files(embedmatrix, EMBED_FILE_LOC, EMBED_MAP_FILE_LOC)


# TODO: LOAD EMBED FILE, EMBED MATRIX, AND EMBED MAP TO S3 HERE
# EMBED_MATRIX_SAVE_LOC
# EMBED_FILE_LOC
# EMBED_MAP_FILE_LOC