

= Training
1. embed
2. crop image given bbox
3. create bbox and save bboxes
4. select bbox given item name (dress, shirt, etc)
5. select bbox given other image/images (select same image)
6. data augmentation
7. create attributes vector from text description


= Search
1. create index given embeddingfile
2. search for images given embedding (return image id)


= Util
1. get image metadata given imageid, itemid


= Data Refresh
1. Run scrapers / hit apis and download metadata
2. Download images and save images/metadata
3. embed new images
4. reembed all images
5. update metadata
6. create embeddingfile for search