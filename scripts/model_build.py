''' 

Run the deep learning model that creates the embeddings


'''

import copy
import itertools
import glob
import time

import numpy as np
import pandas as pd
from PIL import Image
from sklearn.model_selection import train_test_split
import torch.nn as nn
import torch
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms
import torchvision.models as models


from fashionsnap.embedding.object import LocalEmbeddingObject, EmbeddingObject
from fashionsnap.embedding import model as fsmodel




ATTRIBUTE_DATA = '/home/taylor/Programs/fashionsnap/attributedf.csv'
ATTRIBUTE_IMAGE_BASE = '/home/taylor/Programs/fashionsnap/images/forever21/photos/'
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def path_to_imageid(path):

    part1 = path.split('/')[-1].split('.')[0]
    part2 = path.split('/')[-2]
    ending = path.split('/')[-1].split('.')[1]

    imageid = '{}-{}.{}'.format(part1,part2,ending)
    return imageid

def create_view_mask(columnlist, view):
    ''' Take list of columns, return mask vector'''
    if view not in ['front','back','side']:
        raise ValueError("View mask can't be created - pass in proper value")

    if view=='front': # 1 if it doesnt start with <b>
        mask = [ int(not i.startswith('<b>')) for i in columnlist]
    elif view=='back': # 1 if it doesnt start with <f>
        mask = [ int(not i.startswith('<f>')) for i in columnlist]
    else:
        mask = [1 for i in columnlist]

    return mask

def apply_view_mask(vector, viewmask):
    newvector = [a*b for a,b in zip(vector,viewmask)]
    return newvector

def train_model(model, criterion, optimizer, dataloaders, epoch_size_dict, scheduler=None, num_epochs=25):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                if scheduler:
                    scheduler.step()
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for inputs, labels in itertools.islice(dataloaders[phase],epoch_size_dict[phase]):
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    # _, preds = torch.max(outputs, 1)

                    # import ipdb;ipdb.set_trace()
                    if phase=='train': outputs = outputs[0] #inception for some reason needs this for now
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                # running_corrects += torch.sum(preds == labels.data)  # do something like match >.7 to 1 and count

            epoch_loss = running_loss / epoch_size_dict[phase]
            # epoch_acc = running_corrects.double() / epoch_size_dict[phase]

            print('{} Loss: {:.4f}'.format(phase, epoch_loss))
            # print('{} Loss: {:.4f} Acc: {:.4f}'.format(phase, epoch_loss, epoch_acc))

            # # deep copy the model
            # if phase == 'val' and epoch_acc > best_acc:
            #     best_acc = epoch_acc
            #     best_model_wts = copy.deepcopy(model.state_dict())

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model


class AttributeDataset(Dataset):
    ''' Input should have a 'fullurl' column '''

    def __init__(self, dataframe, attribute_columns=None,transforms=None, apply_mask=True):
        self.data = dataframe
        self.transforms = transforms
        self.apply_mask = apply_mask
        self.attribute_columns = attribute_columns

        # create masks here - view column needed


    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):

        imgpath = self.data.iloc[idx]['fullpath']
        img = Image.open(imgpath)
        labels = self.data.iloc[idx,1:][self.attribute_columns].values.astype(np.float32)

        if self.transforms:
            img = self.transforms(img)

        if self.apply_mask:
            pass

        return img, labels




#  PREPARE INPUT DATA
df = pd.read_csv(ATTRIBUTE_DATA)

attribute_columns = df.columns[12:12+53]
num_attributes = len(attribute_columns)

imagelocs = set(glob.glob(ATTRIBUTE_IMAGE_BASE+'*'))
df['fullpath'] = df['url_x'].apply(lambda x: ATTRIBUTE_IMAGE_BASE+path_to_imageid(x))
df = df.loc[df['view'].isin(['front','back','side','flat','full'])] # {'front', 'flat', 'side', 'back', 'full', 'detail'}
df['valid_url'] = df['fullpath'].apply(lambda x:x in imagelocs)
df = df[df['valid_url']==True]

df_train, df_test = train_test_split(df,test_size=.2)

# testpath = df['fullpath'].iloc[0]
# testimg = fsmodel.general_image_loader(testpath)
# labels = df.iloc[0][attribute_columns]
# imgpath = df.iloc[0]['fullpath']




#  CREATE DATA LOADER

# load image and resize / load vector and apply mask
dataloaders = {}
loader = transforms.Compose([
        transforms.Resize((299,299)), #(224,224)
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor()
    ])
train_dataset = AttributeDataset(df_train,attribute_columns=attribute_columns,transforms=loader)
train_dataloader = DataLoader(train_dataset, batch_size=6, shuffle=True, num_workers=3)
test_dataset = AttributeDataset(df_test,attribute_columns=attribute_columns,transforms=loader)
test_dataloader = DataLoader(test_dataset, batch_size=6, shuffle=True, num_workers=3)

dataloaders['train'] = train_dataloader
dataloaders['val'] = test_dataloader

epoch_size_dict = {}
epoch_size_dict['train'] = 20
epoch_size_dict['val'] = 4




#  BUILD MODEL

# model = models.resnet101(pretrained=True)
model = models.inception_v3(pretrained=True)
# model = models.vgg19_bn(pretrained=True)
num_ftrs = model.fc.in_features
model.fc = nn.Linear(num_ftrs, num_attributes)


criterion = nn.BCEWithLogitsLoss()
# criterion = nn.MSELoss()
optimizer = optim.Adam(model.parameters())
# optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)
# scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=7, gamma=0.1)


model = train_model(model, criterion, optimizer, dataloaders, epoch_size_dict, scheduler=None, num_epochs=40)




def sample_run(model,idx,attribute_columns):
    img,lab = train_dataset[idx]
    sig = nn.Sigmoid()
    output = model(img.unsqueeze(0))
    return list(zip(lab, sig(output).squeeze(0), attribute_columns))

import ipdb;ipdb.set_trace()


### TODO
# create repo
# create AMI with images on it
# way to view images/attributes - save to csv?
# way to load model / save model


# https://www.pyimagesearch.com/2018/05/07/multi-label-classification-with-keras/