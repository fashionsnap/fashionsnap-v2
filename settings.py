


import os




# S3_BUCKET = 
# EMBEDDINGFILE = 
# OBJECT_DETECTION_MODEL = 
EMBED_LAMBDA_URL = 'https://l7qm70f3hh.execute-api.us-east-2.amazonaws.com/prod/embed'
S3_IMAGE_PATH = 'website/images/full/'
SIMSERVER_HOST = ''
SIMSERVER_PORT = 9090
TEST_IMAGE_LOC = '/home/taylor/Programs/fashionsnap-v2/data/testimages/'
IMAGES_CSV = '/home/taylor/Programs/fashionsnap/scrapers/shopstyle/fulldata/fulldata-images-2017-08-06.csv'
IMAGE_METADATA_CSV = '/home/taylor/Programs/fashionsnap/scrapers/shopstyle/fulldata/fulldata-metadata-2017-08-06.csv'