
import unittest

from fashionsnap.detection import detector




class TestBBox(unittest.TestCase):

	def test_bbox_overlap(self):
		pass

	def test_bbox_filter(self):
		pass

	def test_crop_all(self):
		pass

	def test_filter_bbox_by_example(self):
		pass

	def test_user_input_image_bboxed(self):
		''' User has selected bounding box and submitted to app '''

		imgid = 'testimg.jpg'

		# percents from top-left
		user_bbox_dict = {}
		user_bbox_dict['top'] = .03
		user_bbox_dict['left'] = .05
		user_bbox_dict['bottom'] = .7
		user_bbox_dict['right'] = .88

		ubboxset = detector.ImageBBoxSet.create_from_user_crop(imgid, bbox_dict)



class TestDetection(unittest.TestCase):

	def test_detection_output(self):
		''' Make sure model runs and gives output in the form we want '''
		pass

	def test_index_to_category(self):
		pass

	def test_stock_image_with_category(self):
		''' Clean image with category provided. Should be 'valid'. End to end test.'''
		pass

	def test_user_input_image(self):
		''' No category, multiple clothing items to detect. End to end test. ImageBBoxSet with have multiple
			bboxes and not be 'valid' '''

		pass


### USE CASES (this should all basically be working [not 3 yet]. Write scripts to do them all)

# 1. single image comes in with user selected bounding box

# 2. single image comes in and we need to create bboxes for all clothes, crop, and save them

# 3. we have a stock photo and category and an in wild photo we need to extract the clothing from

# 4. we have an image of one person and category and we need to bound and crop

# 5. we need to bbox and crop tons of photos



imgid = 'testimg'
# imgpath = '/home/taylor/Programs/fashionsnap/images/samples/dress.jpg'
# imgcat = 'full'
# imageidchunks = [(imgid,imgpath,imgcat)]
# tf_graph_path = '/home/taylor/Programs/fashionsnap-v2/data/models/output_inference_graph-rfcn.pb'


# object_detector = detector.TensorflowObjectDetector(tf_graph_path)
# imagebboxsets = object_detector.batch_bbox(imageidchunks)
# imagebboxsets = detector.filter_valid_imagebboxsets(imagebboxsets)

# import ipdb;ipdb.set_trace()


# these are percents
user_selected_top = .03
user_selected_left = .05
user_selected_bottom = .7
user_selected_right = .88



bboxes = [([user_selected_top,user_selected_left,user_selected_bottom,user_selected_right],None,1.0)]
bboxset = detector.ImageBBoxSet(imgid, bboxes)



bbox_dict = {'top':user_selected_top,'bottom':user_selected_bottom,'left':user_selected_left,'right':user_selected_right}

ubboxset = detector.ImageBBoxSet.create_from_user_crop(imgid,bbox_dict)
import ipdb;ipdb.set_trace()
