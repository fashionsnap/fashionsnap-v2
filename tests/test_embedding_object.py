'''

Test the utilities around the embeddings of objects (not the embeddings themselve)

'''

import os
import unittest

import mockdata
from fashionsnap.embedding.object import LocalEmbeddingObject, LambdaEmbeddingObject, EmbeddingObject
from fashionsnap.embedding.model import VGG






class TestEmbeddingObject(unittest.TestCase, mockdata.MockDataMixin):

	def setUp(self):
		self.embeddingobj = EmbeddingObject()
		self.set_mock_data()

	def test_create_embedding_matrix(self):
		embedmatrix = self.embeddingobj.create_embedding_matrix(self.embedarraylist)

	def test_save_embedding_matrix(self):
		filepath = '/tmp/tmp_embedmatrix.npy'
		embedmatrix = self.embeddingobj.create_embedding_matrix(self.embedarraylist)
		self.embeddingobj.save_embedding_matrix(embedmatrix,filepath)
		
		if os.path.isfile(filepath):
			os.remove(filepath)
		else:
			raise Error("Error: %s file not found" % filepath)





# embedarraylist = mockdata.fakedata.embedarraylist
# filepath = '/tmp/tmp_embedmatrix.npy'

# embeddingobj = EmbeddingObject()
# embedmatrix = embeddingobj.create_embedding_matrix(embedarraylist)
# embeddingobj.save_embedding_matrix(embedmatrix,filepath)



vgg = VGG()
embedobj = LocalEmbeddingObject(vgg)
embedding = embedobj.create_embedding(imgid)

import ipdb;ipdb.set_trace()