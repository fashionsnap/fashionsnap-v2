
import os
import unittest
import random

import numpy as np

import mockdata
from fashionsnap.embedding.object import LocalEmbeddingObject, LambdaEmbeddingObject
from fashionsnap.similarity.search import NMSLIBSearchFile, SearchFile






class TestSearchFiles(unittest.TestCase, mockdata.MockDataMixin):

	def setUp(self):
		self.set_mock_data()

	def test_nmslib_searchfile_embeddingfile_create(self):
		filepath = '/tmp/tmp_embeddingfile.txt'
		searchfile = NMSLIBSearchFile()
		searchfile._create_search_embedding_file(self.embedmatrix,filepath)

		if os.path.isfile(filepath):
			os.remove(filepath)
		else:
			raise Error("Error: %s file not found" % filepath)

	def test_nmslib_searchfile_embeddingid_map_create(self):
		mapfilepath = '/tmp/tmp_embeddingid_map.json'
		searchfile = NMSLIBSearchFile()
		searchfile._create_embeddingid_map(self.embedmatrix,mapfilepath)

		if os.path.isfile(mapfilepath):
			os.remove(mapfilepath)
		else:
			raise Error("Error: %s file not found" % mapfilepath)





# embed_length = 20
# embedmatrix = []
# for imgid in ['11111','22222','33333']:
# 	row = [imgid]
# 	row.extend([random.random() for _ in range(embed_length)])
# 	embedmatrix.append(row)
	
# embedmatrix = np.array(embedmatrix)

# filepath = '/tmp/tmp_embeddingfile.txt'
# searchfile = NMSLIBSearchFile()
# searchfile._create_search_embedding_file(embedmatrix,filepath)


# mapfilepath = '/tmp/tmp_embeddingid_map.json'
# emap = searchfile._create_embeddingid_map(embedmatrix,mapfilepath)
# import ipdb;ipdb.set_trace()