from setuptools import setup

setup(name='fashionsnap',
      version='0.2',
      description='Fashion Recognition',
      url='http://github.com/thess24',
      author='Taylor Hess',
      author_email='',
      license='MIT',
      packages=['fashionsnap'],
      zip_safe=False)