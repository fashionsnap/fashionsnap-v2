


import abc
import csv
import collections
import json

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

from fashionsnap.similarity.nmslib_protocol import QueryService
from fashionsnap.similarity.nmslib_protocol import ttypes

from fashionsnap.util import genutil
from settings import SIMSERVER_HOST, SIMSERVER_PORT





def rollup_image_scores_to_item_level(imagescoredict):
    ''' Takes a list of dicts that contain image metadata with similarity scores. 
        Rolls this up to item level and picks representative image.'''
    
    # group by itemid
    itemimagesdict = collections.defaultdict(list)
    for m in imagescoredict: 
        itemimagesdict[m['itemid']].append(m)

    def take_top_score_item(imagedictlist):
        image_dict =  sorted(imagedictlist, key= lambda x:x['score'], reverse=True)[0]
        return image_dict

    # take top scoring image from each item
    finalitemslist = []
    for k,v in itemimagesdict.items():
        image_dict = take_top_score_item(v)
        finalitemslist.append(image_dict)

    return finalitemslist



class SimilaritySearchConnection(abc.ABC):
    
    def __init__(self, embeddingid_map):
        self.embeddingid_map = embeddingid_map # a dict of {'embedid':'imageid'}

    @abc.abstractmethod
    def query_sim_server(self, embeddingarray, relatedcount = 100):
        pass


class MockSearchConnection(SimilaritySearchConnection):

    def __init__(self):
        super().__init__(None) # no map

    def query_sim_server(self, embeddingarray, relatedcount = 100):
        imageids = ['9066aa262c16e7bbcd85749c66a1855b', '6f86b8b3b091425d0e46223b62f65d19', '619a24429fd8c7c626c160bc37e3aba2', '891a8f061cb91392e0a3a196b7a3c2ac', 'a98bd0dfb71a6c6994fa1e2b3d18b89e', 
        'b1b57ccd9b94280f3b54918976795070', '2b73e754e32d6f2864b1955a78c001c8', '199cb0b4e458bfc1aeab3aadbe2c4dfe', '8477258570e2b213f1889e7f07b96d98', 'a332201c85a38b6d6bad88dbc5f679be', 
        'cb26653e8a1a17d24775b47098c7b824', 'd1dbf6c40e3f175ea0d965998fb4a4c6', 'e50c1a6ab87746926611fbbbcb27536b', '481612960680851a584399b725cba303', '056bc0235d1b5ea4d266197040e76e4e', 
        'fda0de3a9b03f052447f67d1ba747bc6', '061882fc40315822e759e8a759814419', '148d45c052db09e2dbdc0173c99aef39', '2a6a668b9015b8b7dfa83dc0b17c2525', '30f63ed7607049324e37613e0e1f6d48', 
        '3d74feee7a4888d362115062d1b0273f', 'fdf983498fd4948be986e7c99ee6e3bd', 'f81e5180ee32cb7526a982d1072dd1ea', '078d1ee4e3fe24c2d24e314fc1d7e27a', '245abf8da114e4a4eda5c42d89c57674', 
        'bd200cfb136c6cc2f62d246567f1d1c8', 'efb724980099a7addc14f3e580681ae4', '42c24b39f7f56eb285fc9d2de675a67e', '006f5eb938864402507a9c44567efcb6', '6a7b26fde7b8b2a5c6bf71316eeeccde', 
        '7a3978974b2c43afa1829293e5b877da', '95c0c3c65e9ef9bebf1acb07ef1b871c', '6795141c9d2557f4ffbd36ac005aba67', '13b86a773837289e1dadc9064400d3c6', '6cc4eb00faeb27b5556b579dd970e0c9', 
        '6cf03ed8bab1ac757a8ef75601583d5c', 'b9a5ba413a7c828ec812458e07f85658', '7ad46541f2799bd0ae441e978d1edadf', 'fee7a0850001ec1a0e5593074d74f114', '211c0c6724498a7d5c8ff8275eb93b19', 
        '5e36d1ff695d41e93bb8d529642ee732', '7a273d6398e04b48abf804baeb5e49db', '8c1a572c9cd2da6e5c3fba0a49c9605a', 'b692515e2a92ec9a36d1e46dfaf08f1e', 'b9e60f40aae4ac8820d1655769beafcb']
        scores = [random.random() for _ in range(45)]
        imageid_score_dict = dict(zip(imageid,scores))
        
        return imageid_score_dict



# class NumpySearchConnection(SimilaritySearchConnection):
#     ''' Add data stored in large numpy array '''

#     def __init__(self, nparray):




class NMSLIBSearchConnection(SimilaritySearchConnection):


    def __init__(self, embeddingid_map, sim_server_host=SIMSERVER_HOST, sim_server_port=SIMSERVER_PORT):

        if not sim_server_host or sim_server_port:
            raise ValueError('Host and port needed to connect to similarity server')

        self.sim_server_host = sim_server_host
        self.sim_server_port = sim_server_port
        super().__init__(embeddingid_map)


    def query_sim_server(self, embeddingarray, relatedcount = 100):
        ''' Returns {imageid:score, imageid:score} by querying similarity server '''

        embeddingstring = genutil.array_to_embedstring(embeddingarray)

        imageiddict = {}

        try:
            transport = TSocket.TSocket(self.sim_server_host, self.sim_server_port)
            transport = TTransport.TBufferedTransport(transport)
            protocol = TBinaryProtocol.TBinaryProtocol(transport)
            client = QueryService.Client(protocol)
            transport.open()
            res = client.knnQuery(relatedcount, embeddingstring, False, False)
            transport.close()

            for e in res:
                realid = self.embeddingid_map[str(e.id)]
                imageiddict[realid] = e.dist

        except Thrift.TException as tx:
            logging.error(tx.message)


        return imageiddict







class SearchFile(object):
    
    def __init__(self):
        pass

    @abc.abstractmethod
    def _create_search_embedding_file(self,embedmatrix,filepath):
        ''' Take standard embeddings from file and format to necessary form for indexing/loading to server.
            Return the path you saved the file to. '''
        
        return filepath

    @abc.abstractmethod
    def _create_embeddingid_map(self,embedmatrix,mapfilepath):
        embeddingid_map = {}
        return embeddingid_map
        
    def create_embedding_files(self,embedmatrix,filepath, mapfilepath):
        ''' Embedding files contins embeddings, embedding-map file contains imageids '''
        embeddingid_map, mapfilepath = self._create_embeddingid_map(embedmatrix,mapfilepath)
        self._create_search_embedding_file(embedmatrix,filepath)
        return embeddingid_map 


    @abc.abstractmethod
    def merge_embedding_files(self):
        ''' Take an old file/ map and merge a new file/map with it by getting rid of old values
            appending new values '''
        pass


class NMSLIBSearchFile(SearchFile):

    def _create_search_embedding_file(self,embedmatrix,filepath):
        
        # drop first column of ids
        embeddingsonly = embedmatrix[:,1:]

        rows = embeddingsonly.shape[0]
        # write rows to file with spaces between
        with open(filepath,'w+') as f:
            csvwriter = csv.writer(f, delimiter=' ')
            for i in range(rows):
                row = embeddingsonly[str(i)]
                csvwriter.writerow(["{0:.6f}".format(float(i)) for i in row])


    def _create_embeddingid_map(self,embedmatrix,mapfilepath):

        with open(mapfilepath,'w+') as f:
            rows = embedmatrix.shape[0]
            embeddingid_map = dict(zip(range(rows),embedmatrix[:,0]))
            json.dump(embeddingid_map,f)

        return embeddingid_map

