'''

Similarity metrics (mostly for testing)

List of some similarity metrics here
https://github.com/nmslib/nmslib/blob/master/manual/manual.pdf

'''

import functools

import numpy as np
from sklearn.metrics.pairwise import pairwise_distances




def metric_from_vectorlist(vectorlist, metric):
	''' Take list of vectors and return matrix of simlarities '''
	X = np.array(vectorlist)
	distances = pairwise_distances(X,metric=metric,n_jobs=-1)
	return distances

l1_from_vectorlist = functools.partial(metric_from_vectorlist,'l1')
l2_from_vectorlist = functools.partial(metric_from_vectorlist,'l2')
cosine_similarity_from_vectorlist = functools.partial(metric_from_vectorlist,'cosine')


def all_metrics_from_vectorlist(vectorlist):
	metrics = {}
	metrics['l1'] = metric_from_vectorlist(vectorlist,'l1')
	metrics['l2'] = metric_from_vectorlist(vectorlist,'l2')
	metrics['cosine'] = metric_from_vectorlist(vectorlist,'cosine')
	return metrics