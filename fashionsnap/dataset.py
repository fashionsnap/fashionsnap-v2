'''

Dataset for purpose of getting metadata about images and items -- not for training of model


'''

import abc
import logging
import glob

import pandas as pd




class ImageDataset(abc.ABC):
    ''' Class for connection to dataset and accessing info '''

    def __init__(self):
        self._connect()

    @abc.abstractmethod
    def _connect(self):
        pass

    @abc.abstractmethod
    def get_image_info(self, imageid):
        ''' Takes list of imageids and returns full metadata '''
        pass

    @abc.abstractmethod
    def get_item_images(self, itemid):
        ''' Takes itemid and returns list of imageids '''
        pass



class CSVImageDataset(ImageDataset):

    def __init__(self,linksdict):
        
        if 'imagefileloc' in linksdict:
            self.imagefileloc = linksdict['imagefileloc']
        else:
            raise ValueError('imagefileloc needed')

        if 'itemfileloc' in linksdict:
            self.itemfileloc = linksdict['itemfileloc']
        else:
            raise ValueError('itemileloc needed')

        super().__init__()


    def _connect(self):
        ''' Join csvs to get one dataset object '''
        images_cols = ['itemid','imageid','imageurl']

        imagesdf = pd.read_csv(self.imagefileloc, names=images_cols, usecols=['itemid','imageid'])
        self.item_image_map = imagesdf.groupby('itemid')['imageid'].apply(list)

        imagesdf.set_index('imageid',inplace=True)
        imagesdict = imagesdf.to_dict(orient='index')
        self.imagesdict = imagesdict
        del imagesdf

        metadata_cols = ['itemid','url','price','saleprice','name','preowned','description','locale','retailerid',\
                         'retailername','brandid','brandname','currency','inserttime','category']

        itemsdf = pd.read_csv(self.itemfileloc, names=metadata_cols, usecols=['itemid','name','url','price','saleprice', 'description', 'brandname', 'retailername'])
        itemsdf.set_index(['itemid'], inplace=True)
        itemsdict = itemsdf.to_dict(orient='index')
        self.itemsdict = itemsdict
        del itemsdf

        
    def get_image_info(self, imageidlist):
        retlist = []
        
        for image in imageidlist:
            imagedata = {'imageid':image}
            try:
                imagedata.update(self.imagesdict[image])
            except:
                print('imagedata not found: {}'.format(image))
                continue

            itemid = int(imagedata['itemid'])
            imagedata.update(self.itemsdict[itemid])
            retlist.append(imagedata)

        return retlist


    def get_item_images(self, itemid):
        imagelist = []
        itemid = int(itemid)

        try:
            imagelist = self.item_image_map[itemid]
        except:
            print('itemid doesnt exist')

        return imagelist


    def get_image_attributes(self, imageid):
        pass

    def get_item_attributes(self, itemid):
        ''' Basically just 'OR' all image attributes '''
        pass  

    def get_bbox(self, imageid):
        pass