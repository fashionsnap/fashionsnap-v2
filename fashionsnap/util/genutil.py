
import glob

import numpy as np
import pandas as pd
from PIL import Image
from torch import nn

from fashionsnap.similarity.metrics import all_metrics_from_vectorlist
from settings import TEST_IMAGE_LOC




# pd.set_option('max_columns', 20)
# pd.set_option('max_rows', 20)
# pd.set_option('precision', 3)
pd.options.display.float_format = '{:.2f}'.format



def bytesio_to_nparray(imfile):
    image = Image.open(imfile)
    (im_width, im_height) = image.size

    return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)


def crop_image(img, croparray):
    ''' Takes image and array of crop tuples [(top,left,bottom,right)] and outputs list of cropped images '''

    npimg = bytesio_to_nparray(img)
    imglist = []

    for i in croparray:
        top = int(i[0])
        left = int(i[1])
        bottom = int(i[2])
        right = int(i[3])

        imgcrop = npimg[top:bottom,left:right]
        cropim = Image.fromarray(imgcrop)
        imglist.append(cropim)

    return imglist


def attribute_vector_from_text(description, pose=None):
    ''' generate attribute vector from description and pose'''

    pass


def array_to_embedstring(embedarray):
    ''' Take array and turn into embed string '''
    embedstring = ' '.join(["{:.2f}".format(i) for i in embedarray])
    return embedstring


def embedstring_to_array(embedstring):
    return np.array([float(i) for i in embedstring.split(' ')])


# def modify_filename_pre_ext(fname,string_to_add,add_dash=True):
#     ''' Add string after filename and before extension.
#         So (abcd.jpg,1) becomes abcd-1.jpg
#      '''
#     if add_dash:
#         string_to_add = "-{}".format(string_to_add)

#     extension = 
#     filename_wo_ext =
#     newfilename =  "{}{}.{}".format(filename_wo_ext,string_to_add,extension)
#     return newfilename


def filename_to_path(filename, basepath):
    ''' Take filename and basepath, and return full path to image'''

    imagepath = "{}{}".format(basepath, filename)
    return imagepath


def filename_to_croppath(filename):
    basepath = S3_IMAGE_CROP_BASE
    imagepath = filename_to_path(filename, basepath)
    return imagepath


def filename_to_fullpath(filename):
    # TODO: make this work for local files as well
    basepath = S3_IMAGE_FULL_BASE
    imagepath = filename_to_path(filename, basepath)
    return imagepath


def path_to_filename(filepath):
    return filepath.split('/')[-1]



def model_summary(m, input_size):
    def is_listy(x): return isinstance(x, (list,tuple))
    def register_hook(module):
        def hook(module, input, output):
            class_name = str(module.__class__).split('.')[-1].split("'")[0]
            module_idx = len(summary)

            m_key = '%s-%i' % (class_name, module_idx+1)
            summary[m_key] = OrderedDict()
            summary[m_key]['input_shape'] = list(input[0].size())
            summary[m_key]['input_shape'][0] = -1
            if is_listy(output):
                summary[m_key]['output_shape'] = [[-1] + list(o.size())[1:] for o in output]
            else:
                summary[m_key]['output_shape'] = list(output.size())
                summary[m_key]['output_shape'][0] = -1

            params = 0
            if hasattr(module, 'weight'):
                params += torch.prod(torch.LongTensor(list(module.weight.size())))
                summary[m_key]['trainable'] = module.weight.requires_grad
            if hasattr(module, 'bias') and module.bias is not None:
                params +=  torch.prod(torch.LongTensor(list(module.bias.size())))
            summary[m_key]['nb_params'] = params

        if (not isinstance(module, nn.Sequential) and
           not isinstance(module, nn.ModuleList) and
           not (module == m)):
            hooks.append(module.register_forward_hook(hook))

    summary = OrderedDict()
    hooks = []
    m.apply(register_hook)

    if is_listy(input_size[0]):
        x = [Variable(torch.rand(3,*in_size)) for in_size in input_size]
    else: x = [Variable(torch.rand(3,*input_size))]
    m(*x)

    for h in hooks: h.remove()
    return summary

# summary = model_summary(new_model,(3,299,299))

def get_sample_imagepaths():

    imagepaths = sorted(glob.glob('{}*'.format(TEST_IMAGE_LOC)))

    imageids = [i.split('/')[-1] for i in imagepaths]
    imageids = [i.split('.')[0] for i in imageids]

    return imageids, imagepaths


def create_sampleimage_metrics(model, image_loader, print_metrics=True):
    ''' 
    model: model/function that can be called to get embedding
    image_loader: fxn that takes path and returns image to run through model

    '''
    imageids, imagefiles = get_sample_imagepaths()
    images = [image_loader(i) for i in imagefiles]
    embeds = [model(i).view(-1).numpy() for i in images]
    metrics = all_metrics_from_vectorlist(embeds)

    for i in metrics.keys():
        df = pd.DataFrame(metrics[i])
        df.columns = imageids
        df['names'] = imageids
        df.set_index('names', inplace=True)
        metrics[i] = df
        if print_metrics:
            print("========",i,"========")
            print(df)

    return metrics