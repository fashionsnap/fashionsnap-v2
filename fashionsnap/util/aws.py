

import io
import logging

import boto3

from pyspark import SparkContext, SparkConf





try:
    s3_client = boto3.client("s3")
    bucket = s3_client.Bucket('fashionsnap')
    logging.info("Successfully connected to S3 client")
except:
    logging.error("Couldn't connect to S3 client")




def load_from_s3_or_load(filepath):
    ''' If local filepath, use that. If S3 path, download. '''

    if filepath.startswith('s3'):
        # dl from S3 and load into memory
        obj = io.BytesIO()
        
        try:
            bucket.download_fileobj(filepath, obj)
        except ClientError as e:
            logging.warn('File {} was not found on S3. Creating one now in memory'.format(filepath))
        except Exception:
            logging.fatal('Major error in program accessing data on S3') 

        obj.seek(0)

    else:

        try:
            # open file
            obj = open(filepath,'rb')
        except:
            logging.warn("Couldn't open file")

        return obj


def save_file_to_s3_or_save(file, filename, acl="public-read", reduced_redundancy=True):

    try:
        bucket.upload_fileobj(file, filename)#, ExtraArgs={"ACL": acl,"ContentType": file.content_type})

    except Exception as e:
        # This is a catch all exception, edit this part to fit your needs.
        print(e)
        # logging.error("Something Happened: ", str(e))
        # return e


def get_spark():
    conf = SparkConf()#.setMaster(master)
    sc = SparkContext(conf=conf)

    logging.info("Got Spark context")

    return sc