
import abc
from collections import OrderedDict

import torch
from torch.autograd import Variable
import torch.nn as nn
import torchvision.transforms as transforms
import torchvision.models as models
from PIL import Image

from fashionsnap.similarity.metrics import all_metrics_from_vectorlist, cosine_similarity_from_vectorlist
from fashionsnap.util.genutil import get_sample_imagepaths, create_sampleimage_metrics


import numpy as np
np.set_printoptions(precision=2)
np.set_printoptions(suppress=True)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")




def general_image_loader(image_name, imsize=(224,224)):

    loader = transforms.Compose([
        transforms.Resize(imsize),
        transforms.ToTensor()
    ])  

    image = Image.open(image_name)
    # fake batch dimension required to fit network's input dimensions
    image = loader(image).unsqueeze(0)
    return image.to(device, torch.float)



def get_embedding(model,layer,img):

    my_embedding = torch.zeros((1,layer.out_features))
    # 4. Define a function that will copy the output of a layer
    def copy_data(m, i, o):
        # print(o.data)
        # print(o.data.shape)
        my_embedding.copy_(o.data)
    # 5. Attach that function to our selected layer
    h = layer.register_forward_hook(copy_data)
    # 6. Run the model on our transformed image
    model(img)

    # import ipdb;ipdb.set_trace()
    # 7. Detach our copy function from the layer
    h.remove()
    # 8. Return the feature vector
    return my_embedding




class FeatureExtractor(nn.Module):
    def __init__(self, submodule, extracted_layers = None, up_to = None):
        '''  
            extracted_layers: list of layer names or int to extract embeddings for
            up_to: layer name or int of layer to run up to

            Example:
            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            myresnet = models.resnet18(pretrained = True)
            extract_list = ["conv1","layer1","avgpool"]
            up_to = 'fc'
            # exact_list=[2,4,6]
            model = FeatureExtractor(myresnet, extract_list, up_to = up_to)
            a = torch.randn(1,3,256,256)
            x = model(a)

            This extracts multiple layers from the model. up_to='fc' is needed so you don't get
            an error - not sure why.

        '''

        super(FeatureExtractor,self).__init__()
        self.submodule = submodule
        self.extracted_layers = extracted_layers
        self.up_to = up_to


    def forward(self, x):
        outputs = []
        skip = False

        for i, (name, module) in enumerate(self.submodule._modules.items()):
            if name == self.up_to or i == self.up_to or skip: 

                skip = True
                continue

            x = module(x)

            if name in self.extracted_layers or i in self.extracted_layers:
                outputs.append(x)

        return outputs




class ModelBase(abc.ABC):

    def __init__(self):
        self._create_model()
        self._create_loader()

    def __call__(self, img):
        return self.get_embedding(img)

    def _create_loader(self):
        self.loader = transforms.Compose([
            transforms.Resize(self._image_size),
            transforms.ToTensor()
        ])

    def load_image(self,imagepath):
        image = Image.open(imagepath)
        # fake batch dimension required to fit network's input dimensions
        image = self.loader(image).unsqueeze(0)
        return image.to(device, torch.float)


    @abc.abstractmethod
    def get_embedding(self, img):
        ''' Standard interface for use in embedding objects.
            Takes image path
            Return embedding vector as numpy array '''
        pass

    @abc.abstractmethod
    def _create_model(self):
        ''' Define the model and set '''
        pass


class VGG(ModelBase):

    _image_size = (224,224)

    def _create_model(self):
        model = models.vgg19_bn(pretrained=True)
        layer = model._modules.get('classifier')._modules.get('0')
        self.model = model
        self.layer = layer

    def get_embedding(self, img):
        self.model.eval()
        embedding = get_embedding(self.model,self.layer,img)
        return embedding


class InceptionV3(nn.Module):
    
    _image_size = (299,299)

    def _create_model(self):
        pass

    def get_embedding(self,img):
        pass


class Resnet18(nn.Module):
    
    _image_size = (299,299)

    def _create_model(self):
        pass

    def get_embedding(self,img):
        pass






# myresnet = models.resnet18(pretrained = True)
# extract_list = ["avgpool"]
# up_to = 'fc'
# model = FeatureExtractor(myresnet, extract_list, up_to = up_to)
# embeds = [model(i) for i in images]




# model = models.inception_v3(pretrained=True)
# new_model = nn.Sequential(*list(model.children())[:-1])


# model.eval()
# new_model.eval()

# with torch.no_grad():
#     print(myim.shape)
#     # out = model(myim)




# resnet = models.resnet18(pretrained=True)
# f = torch.nn.Sequential(*list(resnet.children())[:6])
# layer = resnet._modules.get('avgpool')
# embed = get_embedding(resnet,layer,myim)


# model_ft = models.inception_v3(pretrained=True)
# # num_ftrs = model_ft.fc.in_features
# # model_ft.fc = nn.Linear(num_ftrs, 2)






# vgg = VGG()




# create_sampleimage_metrics(vgg, general_image_loader)

# import ipdb;ipdb.set_trace()
