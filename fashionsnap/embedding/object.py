
import abc
import json
import requests

import numpy as np

from fashionsnap.util import genutil
from settings import S3_IMAGE_PATH, EMBED_LAMBDA_URL


class EmbeddingObject(object):
	''' Wrapper around models that contain helper functions to create embeddings, save objects, etc. 
		Note this is not an abc.ABC class because we need to call the staticmethods from base class '''


	def __init__(self):
		pass

	@abc.abstractmethod
	def create_embedding(self, imageid):
		''' Get embedding in array format to be passed to Search objects for querying.
			Take the imageid, return an embeddingarray.
		 '''
		pass

	def embed_all(self, imageidlist):
		''' Take list of imageids, embed, and return (imageid,np.array of embeddings)'''

		embedarraylist = []
		
		for imageid in imageidlist:
			embedarray = self.create_embedding(imageid)
			embedarraylist.append((imageid,embedarray))

		return embedarraylist

	@staticmethod
	def create_embedding_matrix(embedarraylist):
		''' Write list of [(imageid, embeddings),(),()] to numpy matrix
			with first column as id and the rest as the embeddings '''
		flatlist = []
		for arraylist in embedarraylist:
			newrow = [arraylist[0]]
			newrow.extend(arraylist[1])
			flatlist.append(newrow)

		matrix = np.array(flatlist)
		return matrix


	@staticmethod
	def save_embedding_matrix(embedarrayobject,filepath):

		with open(filepath,'wb') as f:
			np.save(f,embedarrayobject)






class LocalEmbeddingObject(EmbeddingObject):

	def __init__(self, model):
		self.model = model
		super().__init__()

	def create_embedding(self, imageid):
		# get image
		imagepath = gentuil.filename_to_fullpath(imageid)
		image = model.load_image(imagepath)
		
		# run through model
		embeddingarray = model.get_embedding(imagepath)

		return embeddingarray




class LambdaEmbeddingObject(EmbeddingObject):

	def __init__(self, embed_url=EMBED_LAMBDA_URL):
		
		if not embed_url:
			raise ValueError('Url of lambda needed')

		self.embed_url = embed_url
		super().__init__()

	def create_embedding(self, imageid):
		
		imageobjectpath = '{}{}'.format(S3_IMAGE_PATH,imageid)
		values = {"imageid":imageobjectpath}
		r = requests.post(self.embed_url, data=json.dumps(values))

		if r.status_code != 200:
			print(r.status_code)
			# print(r.json()['message'])

		# need to have embedding string to send via json loads
		embeddingstring = r.json()['embedstring'] 

		embeddingarray = genutil.embedstring_to_array(embeddingstring)
		return embeddingarray