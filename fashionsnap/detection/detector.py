
import abc
import io
import logging

import numpy as np
from PIL import Image
import tensorflow as tf

from fashionsnap.util.genutil import bytesio_to_nparray, filename_to_croppath, filename_to_fullpath
from fashionsnap.util.aws import load_from_s3_or_load, save_file_to_s3_or_save


# Bbox = collections.namedtuple('Bbox',['top','left','botom','right'])


def filter_valid_imagebboxsets(imagebboxsetlist):
    return list(filter(lambda x:x.valid ,imagebboxsetlist))


class ImageBBoxSet(object):

    '''    To get final bbox:
            0. Only take above score of 85%
            1. see if its the only bbox in category. if yes, then accept as bbox
            2. if multiple in category and overlap - not implemented (just accept top % in category)
            3. if multiple in category and no overlap - not implemented
            4. if none in category, but in complemntary cat, take that and note
            5. expand bbox by 5% in all categories, 2% in glasses and shoes - all directions

            If no bbox found, take whole image (not currently implemented)
    '''

    def __init__(self, imageid, bboxes, category=None, run_filters=True):
        self.imageid = imageid
        self.bboxes = bboxes # [([top.left,bottom,right], class, score)]
        self.category = category
        self.valid = False # valid if only 1 bbox exists
        self.expanded = False # expand only once
        self.errors = None
        self.crop_images = []
        self.crop_ids = []

        if run_filters:
            self.run_bbox_filter()

        self._validity_update()


    @classmethod
    def create_from_user_crop(cls, imageid, bbox_dict):
        ''' bbox_dict = {'top':xxx,'left':xxx,'bottom':xxx,'right':xxx} '''

        top = bbox_dict['top']
        left = bbox_dict['left']
        bottom = bbox_dict['bottom']
        right = bbox_dict['right']

        bboxes = [([top,left,bottom,right],None,1.0)]
        return cls(imageid, bboxes, run_filters=False)


    def _validity_update(self):

        if len(self.bboxes) == 1:
            if not self.category:
                self.valid = True
            if self.category == self.bboxes[0][1]:
                self.valid = True


    def run_bbox_filter(self):

        self.filter_bboxes_by_category()
        self.filter_overlap()
        self.expand_bboxes()



    @staticmethod
    def bbox_percent_to_pixel(height,width,bbox):

        bbox_top = int(height*bbox[0])
        bbox_left = int(width*bbox[1])
        bbox_bottom = int(height*bbox[2])
        bbox_right = int(width*bbox[3])

        return bbox_top,bbox_left,bbox_bottom,bbox_right


    @staticmethod
    def crop(npimg, bbox):
        ''' Take (imagepath, bboxpair), crop, and save to crop folder '''

        height,width,_ = npimg.shape
        bbox_top,bbox_left,bbox_bottom,bbox_right = ImageBBoxSet.bbox_percent_to_pixel(height,width,bbox)
        
        imgcrop = npimg[bbox_top:bbox_bottom,bbox_left:bbox_right]
        cropim = Image.fromarray(imgcrop)
        cropobj = io.BytesIO()
        cropim.save(cropobj,"JPEG")
        cropobj.seek(0)

        return cropobj


    def crop_all(self, save=False, keep_in_memory=True):
        ''' load image, crop based on bboxes existing, and either save or keep in memory '''

        if not save and not keep_in_memory:
            # if both are False, raise error
            raise ValueError('Need to either save or keep in memory')

        imagepath = filename_to_fullpath(self.imageid)

        imageobj = load_from_s3_or_load(imagepath)
        npimg = bytesio_to_nparray(imageobj)

        for i, bbox in enumerate(self.bboxes):

            cropfilename = modify_filename_pre_ext(self.imageid,i)
            cropobj = ImageBBoxSet.crop(npimg, bbox[0])

            if save:
                croppath = filename_to_croppath(cropfilename)
                save_file_to_s3_or_save(cropobj, croppath)

            if keep_in_memory:
                self.crop_images.append(cropobj)
                self.crop_ids.append(cropfilename)

        return self.crop_ids


    def filter_bbox_by_example(self, otherimagebboxset):
        ''' Update bboxes to filter by comparing to other image bbox set object '''
        
        # crop all and keep in memory

        # use simple algo to compare

        # if a certain similarity threshold is reached, get rid of all other bboxes
        self._validity_update()

        raise NotImplementedError()


    def filter_bboxes_by_category(self):
        ''' keep only bboxes that match given category'''
        if not self.category:
            return

        bboxes = self.bboxes
        self.bboxes = list(filter(lambda x:x[1]==self.category, bboxes))

        if not self.bboxes:
            # if exact category match isnt found, look for similar, easily confused categories
            # and take top one

            compcategories = ['top','full','outerwear']
            if self.category in compcategories:
                bboxes = filter(lambda x:x[0] in compcategories, bboxes)
                if bboxes:
                    # if sim category exists (but real doesnt, take top of other category)
                    self.bboxes = [bboxes[0]]
                    self.errors = 'No bbox with exact category, using similar'


        self._validity_update()


    def filter_overlap(self):
        # TODO: finish this. its not stricly necessary so moving on for now

        ''' If two bboxes have large overlapping area, just take one of them '''
        # rev_bboxes = reversed(self.bboxes)
        # max_overlap = .5
        # raise NotImplementedError()
        pass


    def expand_bboxes(self):
        ''' Add a little padding to bboxes '''

        if self.expanded:
            return

        def add_padding(bbox, category):
            if self.category in ['shoes', 'glasses']:
                padding = .02
            else:
                padding = .05

            top = max(bbox[0]-padding,0.0)
            left = max(bbox[1]-padding,0.0)
            bottom = min(bbox[2]+padding,1.0)
            right = min(bbox[3]+padding,1.0)
            return [top,left,bottom,right]

        self.expanded = True
        self.bboxes = [(add_padding(i[0],i[1]),i[1],i[2]) for i in self.bboxes]

    @staticmethod
    def calculate_bbox_area(bbox):
        top,left,bottom,right = bbox[0],bbox[1],bbox[2],bbox[3]
        area = (right-left) * (bottom-top)
        return area

    @staticmethod
    def calculate_bbox_overlap_area(bbox1,bbox2):
        # https://silentmatt.com/rectangle-intersection/
        # https://stackoverflow.com/questions/306316/determine-if-two-rectangles-overlap-each-other?rq=1

        if (bbox1[1] < bbox2[3] and bbox1[3] > bbox2[1] and bbox1[0] > bbox2[2] and bbox1[2] < bbox2[0]):
            # intersection
            overlap_top = max(bbox1[0],bbox2[0])
            overlap_left = max(bbox1[1],bbox2[1])
            overlap_bottom = min(bbox1[2],bbox2[2])
            overlap_right = min(bbox1[3],bbox2[3])

            area = (overlap_right - overlap_left) * (overlap_bottom - overlap_top)

        else:
            # no intersection 
            area = 0

        return area

    @staticmethod
    def calculate_bbox_overlap(bbox1,bbox2):
        # take total area of both boxes
        # subtract 2*overlapping area
        # this is the non-overlapping area
        # compare this to overlapping area

        bbox1_area = ImageBBoxSet.calculate_bbox_area(bbox1)
        bbox2_area = ImageBBoxSet.calculate_bbox_area(bbox2)
        total_area = bbox1_area + bbox2_area

        overlap_area = ImageBBoxSet.calculate_bbox_overlap_area(bbox1,bbox2)
        non_overlap_area = total_area - overlap_area

        return overlap_area/non_overlap_area




class ObjectDetector(abc.ABC):

    category_index = {1: {'id': 1, 'name': 'top'},
                      2: {'id': 2, 'name': 'bottom'},
                      3: {'id': 3, 'name': 'full'},
                      4: {'id': 4, 'name': 'outerwear'},
                      5: {'id': 5, 'name': 'glasses'},
                      6: {'id': 6, 'name': 'shoes'},
                      7: {'id': 7, 'name': 'hat'},
                      8: {'id': 8, 'name': 'bag'}}

    def __init__(self, modelpath):
        logging.info("Getting object detection model and loading into memory")
        self.modelpath = modelpath
        self._load_model()
        logging.info("Ready to run object detection with the model...")

    @abc.abstractmethod
    def _load_model(self):
        pass
        
    @abc.abstractmethod
    def batch_bbox(self, imageidchunks):
        pass

    def index_to_category(self, index):
        return self.category_index[index]['name']



class TensorflowObjectDetector(ObjectDetector):


    def _load_model(self):
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            # S3_OBJECT_DETECTION_MODEL = 'rfcn/output_inference_graph-rfcn.pb'
            # fid = download_from_s3(OBJECT_DETECTION_MODEL)
            # '/opt/object-detection.pb'
            with open(self.modelpath,'rb') as fid:
                serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')
        self.detection_graph = detection_graph


    def batch_bbox(self, imageidchunks):
        ''' Should pass in a list of [imageid,imgpath,category] and get back an ImageBBoxSet object '''

        detection_graph = self.detection_graph
        imagebboxsets = []

        with detection_graph.as_default():
            with tf.Session(graph=detection_graph) as sess:
                for imgchunk in imageidchunks:
                    imgid = imgchunk[0]
                    imgpath = imgchunk[1]
                    imgcat = imgchunk[2]

                    imageobj = load_from_s3_or_load(imgpath)

                    try:
                        image_np = bytesio_to_nparray(imageobj)
                    except:
                        logging.warn("{} had a conversion to nparray error".format(imgid))
                        continue

                    image_np_expanded = np.expand_dims(image_np, axis=0)
                    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
                    boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
                    scores = detection_graph.get_tensor_by_name('detection_scores:0')
                    classes = detection_graph.get_tensor_by_name('detection_classes:0')
                    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

                    (boxes,scores,classes,num_detections) = sess.run([boxes,scores,classes,num_detections],feed_dict={image_tensor:image_np_expanded})

                    bboxes = zip(boxes[0].tolist(), classes[0], scores[0])
                    bboxes = filter(lambda x:x[2]>.85,bboxes) # only scores above 85%
                    bboxes = [(i[0],self.index_to_category(int(i[1])),i[2]) for i in bboxes] # convert category index to name

                    imbboxset = ImageBBoxSet(imgid, bboxes, imgcat)
                    imagebboxsets.append(imbboxset)

        return imagebboxsets